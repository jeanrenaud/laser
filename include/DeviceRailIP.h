/************************************************************************************//**
	Copyright (c) LAP GmbH Laser Applikationen. All rights reserved.
	\file	DeviceRailIP.h
	\author	DKa
	\date	2014

	This file contains the declaration of the EtherNetIP::Device::IRail class.
****************************************************************************************/
#ifndef _DEVICE_RAIL_IP_H
/// Avoids multiple inclusion of this file.
#define _DEVICE_RAIL_IP_H _DEVICE_RAIL_IP_H

/*---------------------------------------------------------------------------------------
 included header files
---------------------------------------------------------------------------------------*/
#include "EtherNetIP.h"

/*---------------------------------------------------------------------------------------
 defines and constants
---------------------------------------------------------------------------------------*/
#ifdef DEVICERAILIP_EXPORTS
/// Functions with this prefix are declared as exported
#define RAILIP_API									extern "C" __declspec(dllexport)
#else
/// Functions with this prefix are declared as imported
#define RAILIP_API									extern "C" __declspec(dllimport)
#endif // DEVICERAIL_EXPORTS

/// This defines the RED laser color
#define RAIL_IP_LASER_STATUS_COLOR_RED										0x01
/// This defines the GREEN laser color
#define RAIL_IP_LASER_STATUS_COLOR_GREEN									0x02
/// This defines the BLUE laser color
#define RAIL_IP_LASER_STATUS_COLOR_BLUE										0x03
/// This defines the firmware version bootloader 1 in debug info
#define RAIL_IP_DEBUG_INFO_FW_BOOTLOADER_1									0x00000000
/// This defines the firmware version bootloader 2 in debug info
#define RAIL_IP_DEBUG_INFO_FW_BOOTLOADER_2									0x00000001
/// This defines the firmware version bootloader SVN in debug info
#define RAIL_IP_DEBUG_INFO_FW_BOOTLOADER_SVN								0x00000002
/// This defines the firmware size in debug info
#define RAIL_IP_DEBUG_INFO_FW_SIZE											0x00000003
/// This defines the firmware version Xpert 1 in debug info
#define RAIL_IP_DEBUG_INFO_FW_XPERT_1										0x00000004
/// This defines the firmware version Xpert 2 in debug info
#define RAIL_IP_DEBUG_INFO_FW_XPERT_2										0x00000005
/// This defines the firmware version Xpert SVN in debug info
#define RAIL_IP_DEBUG_INFO_FW_XPERT_SVN										0x00000006
/// This defines the Xpert type debug info
#define RAIL_IP_DEBUG_INFO_XPERT_TYPE										0x00000007
/// This defines the Xpert length debug info
#define RAIL_IP_DEBUG_INFO_XPERT_LENGTH										0x00000008
/// This defines the Xpert serial debug info
#define RAIL_IP_DEBUG_INFO_XPERT_SERIAL_NAV									0x00000009
/// This defines the Xpert encoder tolerance debug info
#define RAIL_IP_DEBUG_INFO_XPERT_ENC_TOL									0x0000000A
/// This defines the internal drive offset left in debug info
#define RAIL_IP_DEBUG_INFO_DRIVE_OFFSET_LEFT								0x0000000B
/// This defines the internal drive offset right in debug info
#define RAIL_IP_DEBUG_INFO_DRIVE_OFFSET_RIGHT								0x0000000C
/// This defines the internal drive offset middle in debug info
#define RAIL_IP_DEBUG_INFO_DRIVE_OFFSET_MIDDLE								0x0000000D
/// This defines the configuration of the drives in debug info
#define RAIL_IP_DEBUG_INFO_DRIVE_CONFIG										0x0000000E
/// This defines the laser focus distance in debug info
#define RAIL_IP_DEBUG_INFO_LASER_FOCUS_DISTANCE								0x0000000F
/// This defines the gear scale in debug info
#define RAIL_IP_DEBUG_INFO_GEAR_SCALE										0x00000016
/// This defines the encoder resolution in debug info
#define RAIL_IP_DEBUG_INFO_ENCODER_RESOLUTION								0x00000017
/// This defines the product code in debug info
#define RAIL_IP_DEBUG_INFO_PRODUCT_CODE										0x00000020
/// This defines the product vendor ID in debug info
#define RAIL_IP_DEBUG_INFO_VENDOR_ID										0x00000021
/// This defines the product name in debug info
#define RAIL_IP_DEBUG_INFO_PRODUCT_NAME_1									0x00000022
/// This defines the kunbus module type in debug info
#define RAIL_IP_DEBUG_INFO_MODULE_TYPE										0x00010020
/// This defines the kunbus module serial in debug info
#define RAIL_IP_DEBUG_INFO_MODULE_SERIAL									0x00010021
/// This defines the kunbus module SVN in debug info
#define RAIL_IP_DEBUG_INFO_MODULE_SVN										0x00010022

/*---------------------------------------------------------------------------------------
 namespaces, classes, structures and interfaces
---------------------------------------------------------------------------------------*/
/************************************************************************************//**
	This namespace contains all interfaces neccessary for EtherNetIP communication.
****************************************************************************************/
namespace EtherNetIP
{
	/********************************************************************************//**
		This namespace contains all interfaces neccessary for device communication over
		EtherNet/IP.
	************************************************************************************/
	namespace Device
	{
		/****************************************************************************//**
			This class represents the rail communication over EtherNet/IP.
		********************************************************************************/
		class IRail
		{
		public:
			/************************************************************************//**
				\param[in] Index
					This parameter contains the index to the parameter to receive from
					the device.
				\param[in] Eeprom
					This parameter indicates if the parameter is received from EEPROM or
					volatile memory.
				\param[out] Data
					This parameter contains a pointer to a variable receiving the indexed
					data from the device.
				\return
					TRUE in case of success, FALSE otherwise
				\details
					This method gets the specified debug information from the device.
			****************************************************************************/
			virtual BOOL WINAPI GetDebugInformation(UINT Index, BOOL Eeprom,
				PINT Data)=0;
			/************************************************************************//**
				\param[in] Index
					This parameter contains the index of the drive.
				\param[out] Position
					This parameter contains the pointer to a variable receiving the
					actual drive position in resolution counts. The parameter is optional
					and may be NULL.
				\param[out] Minimum
					This parameter contains the pointer to a variable receiving the
					drive minimum position in resolution counts. The parameter is optional
					and may be NULL.
				\param[out] Target
					This parameter contains the pointer to a variable receiving the
					drive target position in resolution counts. The parameter is optional
					and may be NULL.
				\param[out] Speed
					This parameter contains the pointer to a variable receiving the
					drive speed in resolution counts. The parameter is optional
					and may be NULL.
				\param[out] IsInverted
					This parameter contains the pointer to a variable receiving the
					indication if the drive direction is inverted. The parameter is
					optional and may be NULL.
				\param[out] Online
					This parameter contains the pointer to a variable receiving the
					indication if the drive is online. The parameter is optional and may
					be NULL.
				\param[out] Error
					This parameter contains the pointer to a variable receiving the
					indication if the drive is in an error state. The parameter is
					optional and may be NULL.
				\param[out] Moving
					This parameter contains the pointer to a variable receiving the
					indication if the drive is moving. The parameter is optional and may
					be NULL.
				\param[out] Left
					This parameter contains the pointer to a variable receiving the
					indication if the drive's left limit switch is active. The parameter
					is optional and may be NULL.
				\param[out] Right
					This parameter contains the pointer to a variable receiving the
					indication if the drive's right limit switch is active. The parameter
					is optional and may be NULL.
				\param[out] Valid
					This parameter contains the pointer to a variable receiving the
					indication if the drive's actual position is valid. The parameter
					is optional and may be NULL.
				\return
					TRUE if the drive is installed, FALSE otherwise.
				\details
					This method gets the actual drive state and information.
			****************************************************************************/
			virtual BOOL WINAPI GetDrive(UCHAR Index, PINT Position=NULL,
				PINT Minimum=NULL, PINT Target=NULL, PUINT Speed=NULL,
				PBOOL IsInverted=NULL, PBOOL Online=NULL, PBOOL Error=NULL,
				PBOOL Moving=NULL, PBOOL Left=NULL, PBOOL Right=NULL,
				PBOOL Valid=NULL)=0;
			/************************************************************************//**
				\param[out] Offset
					This parameter contains the pointer to a variable receiving the
					drive's actual offset. The parameter is optional and may be NULL.
				\param[out] Resolution
					This parameter contains the pointer to a variable receiving the
					drive's position resolution in micrometer (0=one micrometer). The
					parameter is optional and may be NULL.
				\param[out] SpeedMaximum
					This parameter contains the pointer to a variable receiving the
					drive's maximum speed. The parameter is optional and may be NULL.
				\param[out] ProjectionDistance
					This parameter
				\return
					The actual drive range in resolution counts. The value depends on the
					drive resolution and is optional and may be NULL.
				\details
					This method gets the actual configuration for all drives.
			****************************************************************************/
			virtual UINT WINAPI GetDriveConfiguration(PINT Offset=NULL,
				PUSHORT Resolution=NULL, PUINT SpeedMaximum=NULL,
				PUINT ProjectionDistance=NULL)=0;
			/************************************************************************//**
				\param[in] Index
					This parameter contains the index of the laser source.
				\param[out] Brightness
					This parameter contains the pointer to a variable receiving the
					laser's brightness in resolution counts. The parameter is optional
					and may be NULL.
				\param[out] Color
					This parameter contains the pointer to a variable receiving the
					laser's color (0=off, 1=red, 2=green, 3=blue). The parameter is
					optional and may be NULL.
				\return
					TRUE in case of success, FALSE otherwise
				\details
					This method gets the actual laser state and information.
			****************************************************************************/
			virtual BOOL WINAPI GetLaser(UCHAR Index, PUINT Brightness=NULL,
				PUCHAR Color=NULL)=0;
			/************************************************************************//**
				\param[out] FocusDistance
					This parameter contains the pointer to a variable receiving the
					focus distance in resolution counts. The parameter is optional
					and may be NULL.
				\return
					The maximum brightness in resolution counts.
				\details
					This method gets the actual configuration for all lasers.
			****************************************************************************/
			virtual UINT WINAPI GetLaserConfiguration(PUINT FocusDistance=NULL)=0;
			/************************************************************************//**
				\return
					The device's serial number
				\details
					This method gets the device's serial number.
			****************************************************************************/
				virtual UINT WINAPI GetSerialNumber(VOID)=0;
			/************************************************************************//**
				\param[out] Booted
					This parameter contains the pointer to a variable receiving the
					indication if the rail system is booted. The parameter is optional
					and may be NULL.
				\param[out] Referencing
					This parameter contains the pointer to a variable receiving the
					indication if the rail system is performing a reference search. The
					parameter is optional and may be NULL.
				\param[out] MovedManually
					This parameter contains the pointer to a variable receiving the
					indication if the rail system is moved manually by a joystick. The
					parameter is optional and may be NULL.
				\param[out] Revision
					This parameter contains the pointer to a variable receiving the
					firmware revision of the rail firmware. The parameter is optional
					and may be NULL.
				\return
					TRUE if the system is online, FALSE otherwise.
				\details
					This method gets the system status of the rail device.
			****************************************************************************/
			virtual BOOL WINAPI GetSystemStatus(PBOOL Booted=NULL,
				PBOOL Referencing=NULL, PBOOL MovedManually=NULL, PUINT Revision=NULL)=0;
			/************************************************************************//**
				\details
					This method deletes the rail object the interface points to.
			****************************************************************************/
			virtual VOID WINAPI Release(VOID)=0;
			/************************************************************************//**
				\param[in] Index
					This parameter contains the index to the parameter to set to the
					device.
				\param[in] Eeprom
					This parameter indicates if the parameter is set to EEPROM or
					volatile memory.
				\param[out] Data
					This parameter contains the data to write to the indexed parameter in
					the device.
				\return
					TRUE in case of success, FALSE otherwise
				\details
					This method sets the debug information to the device.
			****************************************************************************/
			virtual BOOL WINAPI SetDebugInformation(UINT Index, BOOL Eeprom,
				INT Data)=0;
			/************************************************************************//**
				\param[in] Index
					This parameter contains the index of the drive.
				\param[in] Position
					This parameter contains a pointer to a variable containing the new
					indexed drive target position in resolution counts. The parameter is
					optional and may be NULL.
				\param[in] Speed
					This parameter contains a pointer to a variable containing the new
					indexed drive speed in resolution counts. The parameter is optional
					and may be NULL.
				\return
					TRUE in case of success, FALSE otherwise
				\details
					This method sets the new indexed drive parameter.
			****************************************************************************/
			virtual BOOL WINAPI SetDrive(UCHAR Index, PINT Position=NULL,
				PUINT Speed=NULL)=0;
			/************************************************************************//**
				\param[in] Offset
					This parameter contains a pointer to a variable containing the new
					drive offset in resolution counts. The parameter is optional and may
					be NULL.
				\param[in] Resolution
					This parameter contains a pointer to a variable containing the new
					drive resolution in micrometer (0=one micrometer). The parameter is
					optional and may be NULL.
				\param[in] SpeedMaximum
					This parameter contains a pointer to a variable containing the new
					drive speed maximum in resolution counts. The parameter is
					optional and may be NULL.
				\param[in] ProjectionDistance
					This parameter contains a pointer to a variable containing the new
					laser projection distance in resolution counts. The parameter is
					optional and may be NULL.
				\return
					TRUE in case of success, FALSE otherwise
				\details
					This method sets the drive configuration information to the device.
			****************************************************************************/
			virtual BOOL WINAPI SetDriveConfiguration(PINT Offset=NULL,
				PUSHORT Resolution=NULL, PUCHAR SpeedMaximum=NULL,
				PUINT ProjectionDistance=NULL)=0;
			/************************************************************************//**
				\param[in] Index
					This parameter contains the index of the laser source.
				\param[in] Brightness
					This parameter contains a pointer to a variable containing the new
					indexed laser brightness in resolution counts. The parameter is
					optional and may be NULL.
				\details
					This method sets the new indexed laser parameter.
			****************************************************************************/
			virtual BOOL WINAPI SetLaser(UCHAR Index, PUINT Brightness=NULL)=0;
			/************************************************************************//**
				\param[in] BrightnessMaximum
					This parameter contains a pointer to a variable containing the new
					laser brightness maximum in resolution counts. The parameter is
					optional and may be NULL.
				\details
					This method sets the laser configuration information to the device.
			****************************************************************************/
			virtual BOOL WINAPI SetLaserConfiguration(PUCHAR BrightnessMaximum=NULL)=0;
			/************************************************************************//**
				\return
					Returns TRUE in case of success, FALSE otherwise.
				\details
					This method starts a manual reference search.
			****************************************************************************/
			virtual BOOL WINAPI StartReferenceSearch(VOID)=0;
		};

		/****************************************************************************//**
			This class exposes the interface for rail device creation.
		********************************************************************************/
		class IRailControl
		{
		public:
			/************************************************************************//**
				\param[in] Port
					This parameter contains a pointer to the synchronet interface.
				\param[out] DeviceList
					This parameter contains a pointer to the UINT array to write the
					results to. This parameter is optional and may be NULL.
				\param[in] DeviceListSize
					This parameter contains the size of the device list. This parameter
					is optional and may be 0.
				\return
					The number of detected rail devices on the specified communication
					interface. For error information call GetLastError().
				\details
					This method detects the number of rail devices on the specified
					communication interface.
			****************************************************************************/
			virtual UINT WINAPI AutoDetectDevice(IPort* Port, PUINT DeviceList,
				UINT DeviceListSize)=0;
			/************************************************************************//**
				\param[in] Device
					This parameter contains the serial number address of the device.
				\param[in] Port
					This parameter contains a pointer to the EtherNetIP::IPort
					interface that performs the BUS communication to the device.
				\return
					The pointer to the newly created rail device interface if
					successful, NULL otherwise.
				\details
					This method is called to create a rail device at the specified
					serial number on the specified communication interface.
			****************************************************************************/
			virtual IRail* WINAPI CreateDevice(UINT Device, IPort* Port)=0;
			/************************************************************************//**
				\param[out] Minor
					This parameter receives the sub-minor and build version info of the
					DeviceRail.dll. The sub-minor version info is contained in the high
					WORD and the build version info is contained in the low WORD of the
					DWORD pointer. This parameter is optional and may be NULL.
				\return
					The major and minor version info of the DeviceRailIP.dll. The major
					version info is contained in the high WORD and the minor version info
					is contained in the low WORD of the returned DWORD.
				\details
					This method returns version information about the DeviceRailIP.dll
					file.
			****************************************************************************/
			virtual DWORD WINAPI GetVersion(PDWORD Minor)=0;
		};
	} // namespace Device

	/********************************************************************************//**
		\return
			The pointer to the rail control object of the DLL which exports methods
			for cascading rail communication.
		\details
			This method gives access to the rail control object of the DeviceRailIP.dll
			file. From the returned interface pointer, all rail communication interfaces
			can be created.
	************************************************************************************/
	RAILIP_API Device::IRailControl* WINAPI OpenRailControl(VOID);

	/********************************************************************************//**
		\param[in] Rail
			This parameter contains a pointer to the rail interface to be configured.
		\param[in] Language
			This parameter contains the language identifier of the dialog resource to
			open. If the dialog resource for the specified language is not available,
			the default resource (english) is used.
		\param[in] ParentWindow
			This parameter contains the handle to the parent window of the dialog.
		\return
			TRUE if the dailog was closed and the configuration was successful. FALSE if
			the dialog was aborted or the configuration was not successful.
		\details
			This method starts a configuration dialog for non-persistent rail
			configuration. This configuration is reset to default when the rail reboots.
	************************************************************************************/
	RAILIP_API BOOL WINAPI ConfigurationDialog(Device::IRail* Rail, WORD Language,
		HWND ParentWindow);

	/********************************************************************************//**
		\param[in] Rail
			This parameter contains a pointer to the rail interface to be used.
		\param[in] Language
			This parameter contains the language identifier of the dialog resource to
			open. If the dialog resource for the specified language is not available,
			the default resource (english) is used.
		\param[in] ParentWindow
			This parameter contains the handle to the parent window of the dialog.
		\return
			TRUE if the dailog was closed and the properties setting was successful.
			FALSE if the dialog was aborted or the properties setting was not successful.
		\details
			This method starts a properties dialog for persistent rail properties setting
			using the debug interface of the device.
	************************************************************************************/
	RAILIP_API BOOL WINAPI PropertiesDialog(Device::IRail* Rail, WORD Language,
		HWND ParentWindow);
} // namespace EtherNetIP

#endif // _DEVICE_RAIL_IP_H
