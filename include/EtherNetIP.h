/************************************************************************************//**
	Copyright (c) LAP GmbH Laser Applikationen. All rights reserved.
	\file		EtherNetIP.h
	\author		DKa
	\date		2014

	This file contains the descriptions of the hierarchical interface structure of the
	EtherNet/IP API.
****************************************************************************************/
#ifndef _ETHERNET_IP_H
/// Avoids multiple inclusion of this file.
#define _ETHERNET_IP_H _ETHERNET_IP_H

/*---------------------------------------------------------------------------------------
 included header files
---------------------------------------------------------------------------------------*/
// necessary windows header files
#include <Windows.h>

/*---------------------------------------------------------------------------------------
 defines and constants
---------------------------------------------------------------------------------------*/
#ifdef ETHERNETIP_EXPORTS
/// Functions with this prefix are declared as exported
#define ETHERNETIP_API								extern "C" __declspec(dllexport)
#else
/// Functions with this prefix are declared as imported
#define ETHERNETIP_API								extern "C" __declspec(dllimport)
#endif // ETHERNETIP_EXPORTS

/// This is the maximum length of EtherNetIP structure names
#define ETHERNET_IP_MAX_NAME_LENGTH											256

/// This is the maximum length of EtherNetIP adapter names
#define ETHERNET_IP_MAX_ADAPTER_NAME_LENGTH									32

/*---------------------------------------------------------------------------------------
	namespaces, classes, structures, interfaces and functions
---------------------------------------------------------------------------------------*/
/************************************************************************************//**
	This namespace contains all interfaces neccessary for EtherNet/IP communication.
****************************************************************************************/
namespace EtherNetIP
{
	/********************************************************************************//**
		This enumeraion represents the EtherNet/IP device type selection.
	************************************************************************************/
	enum EPortType
	{
		/// The interface uses real EtherNet/IP access.
		REAL_ACCESS=0,
		/// The interface uses virtual BUS access.
		VIRTUAL_ACCESS=1,
	};

	/********************************************************************************//**
		This class exposes the interface for communication on the bus behind the
		EtherNet/IP interface.
	************************************************************************************/
	class IPort
	{
	public:
		/****************************************************************************//**
			\return
				The IP address of the port's device.
			\details
				This method returns the IP address of the port's device.
		********************************************************************************/
		virtual ULONG WINAPI GetAddress(VOID)=0;
		/****************************************************************************//**
			\param[in] Instance
				This parameter contains the object instance. Usually only one instance is
				available.
			\return
				The instance's product code.
			\details
				This method returns the instance's product code. This code describes the
				device in more detail.
		********************************************************************************/
		virtual USHORT WINAPI GetProductCode(UCHAR Instance)=0;
		/****************************************************************************//**
			\param[in] Instance
				This parameter contains the object instance. Usually only one instance is
				available.
			\param[out] NameBuffer
				This parameter contains a pointer to the buffer receiving the product
				name.
			\param[in] NameBufferLength
				This parameter contains the length of the name buffer. Shold be at least
				32.
			\return
				The instance's product name.
			\details
				This method returns the instance's product name.
		********************************************************************************/
		virtual BOOL WINAPI GetProductName(UCHAR Instance, LPTSTR NameBuffer,
			USHORT NameBufferLength)=0;
		/****************************************************************************//**
			\param[in] Instance
				This parameter contains the object instance. Usually only one instance is
				available.
			\return
				The device's serial number.
			\details
				This method returns the serial number of the device.
		********************************************************************************/
		virtual UINT WINAPI GetSerialNumber(UCHAR Instance)=0;
		/****************************************************************************//**
			\return
				The communication type.
			\details
				This method returns the type of communication.
		********************************************************************************/
		virtual EPortType WINAPI GetType(VOID)=0;
		/****************************************************************************//**
			\return
				TRUE if the communication interface is still connected, FALSE if not.
			\details
				This method tests the interface for connection and returns the result in
				a boolean information.
		********************************************************************************/
		virtual BOOL WINAPI IsConnected(VOID)=0;
		/****************************************************************************//**
			\details
				This method releases the interface pointed to.
		********************************************************************************/
		virtual VOID WINAPI Release(VOID)=0;
	};

	/********************************************************************************//**
		This structure implements a EtherNet/IP port description.
	************************************************************************************/
	struct PORT_DESCRIPTION
	{
		/// This variable contains the EtherNet/IP port number.
		UINT PortNumber;
		/// This string contains the service name.
		TCHAR ServiceName[ETHERNET_IP_MAX_NAME_LENGTH];
		/// This string contains the friendly name.
		TCHAR FriendlyName[ETHERNET_IP_MAX_NAME_LENGTH];
	};

	/// This defines a pointer to the port description structure.
	typedef PORT_DESCRIPTION* PPORT_DESCRIPTION;

	/********************************************************************************//**
		This class exposes an interface to an EtherNet/IP gateway created at a network
		adapter.
	************************************************************************************/
	class IGateway
	{
	public:
		/****************************************************************************//**
			\param[out] DescriptionList
				This parameter contains a pointer to a list that receives the description
				of the detected ports on the gateway. If NULL, no information is
				returned.
			\param[in] DescriptionListSize
				This parameter contains the number of entries in the list pointed to by
				DescriptionList. If 0, no information is returned.
			\return
				The number of detected ports on the device.
			\details
				This method is called to detect the number of ports available on the
				gateway.
		********************************************************************************/
		virtual UINT WINAPI AutoDetectPort(PPORT_DESCRIPTION DescriptionList,
			UINT DescriptionListSize)=0;
		/****************************************************************************//**
			\param[in] Port
				This parameter contains the port number to connect with.
			\return
				The pointer to the newly created communication interface if successful,
				NULL otherwise.
			\details
				This method is called to create a communication interface on the gateway
				using the specified port.
		********************************************************************************/
		virtual IPort* WINAPI CreatePort(UINT Port)=0;
		/****************************************************************************//**
			\return
				The devices's IP address.
			\details
				This method returns the IP address of the gateway.
		********************************************************************************/
		virtual ULONG WINAPI GetAddress(VOID)=0;
		/****************************************************************************//**
			\return
				The devices's MAC address.
			\details
				This method returns the MAC address of the gateway.
		********************************************************************************/
		virtual ULONGLONG WINAPI GetMAC(VOID)=0;
		/****************************************************************************//**
			\details
				This method releases the interface pointed to.
		********************************************************************************/
		virtual VOID WINAPI Release(VOID)=0;
	};

	/********************************************************************************//**
		This structure implements an EtherNet/IP gateway description.
	************************************************************************************/
	struct GATEWAY_DESCRIPTION
	{
		/// This variable contains the MAC address.
		ULONGLONG MAC;
		/// This variable contains the IP address.
		ULONG AddressIP;
		/// This string contains the friendly name of the gateway.
		TCHAR FriendlyName[ETHERNET_IP_MAX_NAME_LENGTH];
	};

	/// This defines a pointer to the gateway description structure.
	typedef GATEWAY_DESCRIPTION* PGATEWAY_DESCRIPTION;

	/********************************************************************************//**
		This enumeraion represents the EtherNet/IP adapter type selection.
	************************************************************************************/
	enum EAdapterType
	{
		/// The interface uses virtual BUS access.
		VIRTUAL=0,
		/// The interface uses an EtherNet/IP device for BUS access.
		ETHERNET=1,
	};

	/********************************************************************************//**
		This class exposes the interface to an adapter.
	************************************************************************************/
	class IAdapter
	{
	public:
		/****************************************************************************//**
			\param[out] DescriptionList
				This parameter contains a pointer to a list that receives the description
				of the detected EtherNet/IP gateways on the adapter. If NULL, no
				information is returned.
			\param[in] DescriptionListSize
				This parameter contains the number of entries in the list pointed to by
				DescriptionList. If 0, no information is returned.
			\return
				The number of detected devices on the adapter.
			\details
				This method is called to detect the number of gateways on the adapter.
				The available devices are taken from the registry. For every virtual
				gateway there must be a section in the key 
				"HKEY_CURRENT_USER\Software\LAP GmbH Laser Applikationen\EtherNetIP" of
				the registry named by its MAC address ("xx-xx-xx-xx-xx-xx-xx-xx") and
				containing at least the entries "AddressIP", "ProductCode", "ProductName"
				and "SerialNumber".
		********************************************************************************/
		virtual UINT WINAPI AutoDetectGateway(PGATEWAY_DESCRIPTION DescriptionList,
			UINT DescriptionListSize)=0;
		/****************************************************************************//**
			\param[in] MAC
				This parameter contains the MAC address of the EtherNet/IP gateway to
				connect with. For Type VIRTUAL the address has to be contained in the
				current user branch of the registry. For details see AutoDetectGateway().
			\param[in] AddressIP
				This parameter contains the IP address of the gateway to connect with.
			\return
				The pointer to the newly created gateway interface if successful, NULL
				otherwise.
			\details
				This method is called to create a gateway interface on the adapter.
		********************************************************************************/
		virtual IGateway* WINAPI CreateGateway(ULONGLONG MAC, ULONG AddressIP)=0;
		/****************************************************************************//**
			\return
				The adapter's MAC address.
			\details
				This method returns the MAC address of the adapter.
		********************************************************************************/
		virtual ULONGLONG WINAPI GetMAC(VOID)=0;
		/****************************************************************************//**
			\return
				The adapter type.
			\details
				This method returns the type of the adapter.
		********************************************************************************/
		virtual EAdapterType WINAPI GetType(VOID)=0;
		/****************************************************************************//**
			\details
				This method releases the interface pointed to.
		********************************************************************************/
		virtual VOID WINAPI Release(VOID)=0;
	};

	/********************************************************************************//**
		This structure implements an address description.
	************************************************************************************/
	struct ADDRESS_DESCRIPTION
	{
		/// This variable contains the address.
		ULONGLONG MAC;
		/// This string contains the friendly name of the address.
		TCHAR FriendlyName[ETHERNET_IP_MAX_NAME_LENGTH];
	};

	/// This defines a pointer to the address description structure.
	typedef ADDRESS_DESCRIPTION* PADDRESS_DESCRIPTION;

	/********************************************************************************//**
		This structure implements an adapter description.
	************************************************************************************/
	struct ADAPTER_DESCRIPTION
	{
		/// This variable contains the adapter type.
		EAdapterType Type;
		/// This string contains the friendly name of the adapter.
		TCHAR FriendlyName[ETHERNET_IP_MAX_NAME_LENGTH];
	};

	/// This defines a pointer to the adapter description structure.
	typedef ADAPTER_DESCRIPTION* PADAPTER_DESCRIPTION;

	/********************************************************************************//**
		This class exposes the interface for EtherNet/IP adapter detection and
		connection.
	************************************************************************************/
	class IControl
	{
	public:
		/****************************************************************************//**
			\param[in] Type
				This parameter contains the enumeration of the adapter types implemented.
				If Type is VIRTUAL only one adapter with address 0 and description
				"localhost" is detected.
			\param[out] DescriptionList
				This parameter contains a pointer to a list that receives the description
				of the detected adapters. If NULL, no information is returned.
			\param[in] DescriptionListSize
				This parameter contains the number of entries in the list pointed to by
				DescriptionList. If 0, no information is returned.
			\return
				The number of detected adapters. If no adapters are found, call
				GetLastError() to get extended error information.
			\details
				This method is called to detect the number of adapters of the specified
				type.
		********************************************************************************/
		virtual UINT WINAPI AutoDetectAdapter(EAdapterType Type,
			PADDRESS_DESCRIPTION DescriptionList, UINT DescriptionListSize)=0;
		/****************************************************************************//**
			\param[in] Type
				This parameter contains the enumeration of the adapter types implemented.
			\param[in] MAC
				This parameter contains the MAC address of the adapter to connect with.
				For Type VIRTUAL the address has to be 0.
			\return
				The pointer to the newly created adapter interface if successful, NULL
				otherwise. Call GetLastError() to get extended error information.
			\details
				This method is called to create an interface on the specified adapter.
		********************************************************************************/
		virtual IAdapter* WINAPI CreateAdapter(EAdapterType Type, ULONGLONG MAC)=0;
		/****************************************************************************//**
			\param[in] AdapterTypeList
				This parameter contains a pointer to a list that receives the description
				of the adapter types available. If NULL, no information is returned.
			\param[in] AdapterTypeListSize
				This parameter contains the number of entries in the list pointed to by
				AdapterTypeList. If 0, no information is returned.
			\return
				The number of available adapter types.
			\details
				This method is called to get the number and descriptions of available
				adapter types.
		********************************************************************************/
		virtual UINT WINAPI GetAdapterTypes(PADAPTER_DESCRIPTION AdapterTypeList,
			UINT AdapterTypeListSize)=0;
		/****************************************************************************//**
			\param[out] Minor
				This parameter receives the minor version info of the synchonet DLL.
			\return
				The major version info of the EtherNetIP DLL.
			\details
				This method returns version information about the EtherNetIP.dll file.
		********************************************************************************/
		virtual DWORD WINAPI GetVersion(PDWORD Minor)=0;
	};

	/********************************************************************************//**
		\return
			The pointer to the EtherNet/IP control object of the DLL which exports
			methods for cascading EtherNet/IP communication.
		\details
			This method gives access to the EtherNetIP control object of the
			EtherNetIP.dll file. From the returned interface pointer, all EtherNet/IP 
			communication interfaces can be created.
	************************************************************************************/
	ETHERNETIP_API IControl* WINAPI OpenControl(VOID);

	/********************************************************************************//**
		\param[in] Type
			This parameter contains a variable with the adapter type to use as the
			default type selection.
		\param[in] Selectable
			This parameter contains a variable indicating if the adapter type can be
			selected or is fixed to the one specified in the Type variable.
		\param[in] Language
			This parameter contains the language identifier of the dialog resource to
			open. If the dialog resource for the specified language is not available,
			the default resource (english) is used.
		\param[in] ParentWindow
			This parameter contains the handle to the parent window of the dialog.
		\return
			The pointer to the EtherNetIP::IPort interface if the dialog was closed and
			the interface is available. If the dialog was aborted of the interface
			pointer was not created the method returns NULL.
		\details
			This method starts a dialog for EtherNetIP port selection. The user can
			select an EtherNetIP port access using the available adapter types and
			adapters.
	************************************************************************************/
	ETHERNETIP_API IPort* WINAPI AccessDialog(EAdapterType Type, BOOL Selectable,
		WORD Language, HWND ParentWindow);
} // namespace EtherNetIP

#endif // _ETHERNET_IP_H
